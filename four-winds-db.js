const mysql = require('mysql');

let sqlDB = mysql.createConnection({
    host: 'localhost',
    user: 'connor',
    password: 'password',
    database: 'four-winds'
});

sqlDB.connect();

function getOrders() {
    return new Promise((resolve, reject) => {
        sqlDB.query('SELECT * FROM jos_vm_orders', (error, results) => {
            if(error) {
                reject(err);
            } else {
                let cleanedResults = results.map((row) => {
                    //convert utc timestamp into usable object
                    let date = new Date(row.cdate * 1000);
                    return {
                        "fulfillment_status": "fulfilled",
                        "send_receipt": false,
                        "send_fulfillment_receipt": false,
                        "browser_ip": row.ip_address,
                        "created_at": date.toISOString(),
                        "id": row.order_id,
                        "note": row.customer_note,
                        "order_number": row.order_number,
                        "subtotal_price": row.order_subtotal,
                        "total_price": row.order_total,
                        "total_tax": row.order_tax,
                        "currency": row.order_currency,
                        "line_items": []
                    }
                });
                resolve(cleanedResults);
            }
        });
    });
}

function getOrderCustomers() {
    return new Promise((resolve, reject) => {
        sqlDB.query('SELECT * FROM jos_vm_order_user_info', (error, results) => {
            if (error) {
                reject(err);
            } else {
                let cleanedResults = results.map((row) => {
                    let customerObj = {
                        "customer": {
                            "first_name": row.first_name,
                            "last_name": row.last_name,
                            "state": row.state,
                            "email": row.user_email,
                            "id": row.user_id
                        },
                        "order_id": row.order_id,
                        "email": row.user_email,
                        "address": row.address_1,
                        "city": row.city,
                        "zip": row.zip,
                        "country": row.country,
                        "company": row.company
                    }

                    if(row.phone_1) {
                        customerObj.customer.phone = "+1" + row.phone_1.split(/-|\s/).join('');
                    }
                    return customerObj;
                });
                resolve(cleanedResults);
            }
        });
    });
}

function getOrderItems() {
    return new Promise((resolve, reject) => {
        sqlDB.query('SELECT * FROM jos_vm_order_item', (error, results) => {
            if (error) {
                reject(err);
            } else {
                let cleanedResults = results.map((row) => {
                    return {
                        "item": {
                            "name": row.order_item_name,
                            "title": row.order_item_name,
                            "price": row.product_item_price,
                            "quantity": row.product_quantity,
                            "sku": row.order_item_sku,
                            "product_id": row.product_id
                        },
                        "order_id": row.order_id
                    }
                });
                resolve(cleanedResults);
            }
        });
    });
}

function ordersJsonUpload() {
    return new Promise((resolve, reject) => {
        var orders;
        getOrders().then((results) => {
            orders = results;
            console.log("Got orders...");
            return getOrderCustomers();
        }).then((customers) => {
            for(let customer of customers) {
                for(let i=0;i<orders.length;i++) {
                    let order = orders[i];
                    if (order.id === customer.order_id) {
                        //add customer to order, fields in customer that need to be applied to order obj too
                        order.customer = customer.customer;
                        order.email = customer.email;
                        order.phone = customer.customer.phone;
                        order.shipping_address = {
                            "address1": customer.address,
                            "city": customer.city,
                            "state": customer.customer.state,
                            "first_name": customer.customer.first_name,
                            "last_name": customer.customer.last_name,
                            "phone": customer.customer.phone,
                            "zip": customer.zip,
                            "country": customer.country,
                            "company": customer.company
                        }
                        order.user_id = customer.customer.id;
                    }
                }
            }
            console.log("Got customers...");
            return getOrderItems();
        }).then((lineItems) => {
            for(let order of orders) {
                for(let item of lineItems) {
                    if(item.order_id === order.id) {
                        order.line_items.push(item.item);
                    }
                }
            }
            console.log("Got items...");
            resolve(orders);
        }).catch((e) => {
            reject(e);
        });
    });
}

module.exports = ordersJsonUpload;