const request = require('request');
const jsonOrders = require('./four-winds-db.js');
const fs = require('fs');

let baseUrl = 'https://four-winds-growers.myshopify.com',
    errorOrders = [];

jsonOrders().then((orders) => {
    return new Promise((resolve, reject) => {
        sendOrder(34499);
        function sendOrder(i) {
            var order = orders[i];

            let options = {
                method: 'POST',
                url: baseUrl + '/admin/orders.json',
                body: { "order": order },
                headers: {
                    'Cache-Control': 'no-cache',
                    Authorization: 'Basic M2QzYWFiNDQ0NTFkYzJhMGFkMzcxNDFiMTBkNWNiMzk6ZGJlMWFiYTMyODY4NDFhZDY1MzJiY2RmYThlMmMwOWM=',
                    'Accept': 'application/json'
                },
                json: true
            }

            request(options, (err, resp, body) => {
                if(err || resp.statusCode == "406" || resp.statusCode == "500" || resp.statusCode == "404") {
                    console.log(err);
                    //on error spit out index, to restart from the same spot as before the bug, put the index in the function call above
                    console.log(i, "order index in array");
                    console.log(resp.statusCode);
                    console.log("ERROR ON ORDER ", order.order_number);
                    reject(err);
                } else {
                    if(!body || body.hasOwnProperty('errors')) {
                        if(body) order.errors = body.errors;
                        //any orders not submitted for validation reasons, put order number in log file
                        fs.appendFileSync('log.txt', `, ${order.order_number}`);
                        errorOrders.push(order);
                    }
                    startNewTimeout();
                }
            });

            function startNewTimeout() {
                setTimeout(() => {
                    if(i < orders.length) {
                        sendOrder(i + 1);
                    } else {
                        resolve();
                    }
                }, 500);
            }
        }
    });
}).then(() => {
    console.log("done!");
    if(errorOrders.length) {
        var errorOrderIds = [];
        console.log("\n\nERROR ORDERS\n");
        //display error orders, this is a fallback if fs fails to write
        for(let order of errorOrders) {
            console.log(order.errors);
            console.log(order);
            errorOrderIds += ", " + order.order_number;
        }
        console.log("\n\n\n\n Error order numbers", errorOrderIds);
    }
}).catch((e) => {
    console.log("TOP LEVEL ERROR", e);
});