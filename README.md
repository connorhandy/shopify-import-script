Upload orders via the Shopify admin API.

If you attach customers to the order object they will be added as customers to the store.

For authorization header, you need to make a private app on the Shopify store you're uploading to. You can use this guide to get started https://help.shopify.com/api/tutorials/using-postman. Do a test request in postman and then copy the 'Authorization' header into the request options header. If this it too confusing maybe have someone else help you.

Be wary of your computer falling asleep while orders are sending. 